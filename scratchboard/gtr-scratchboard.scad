$fs = 0.25;

thickness = 3;

wire = 1.1;

coinh = 1.67;
coinr = 8.125;

ledr = 2.5;

d = 1;

tol = 0.25;

module coin() {
    translate([0,0,thickness-1]) cylinder(h=coinh+d, r=coinr+tol);
    translate([0,0,-d/2]) cylinder(h=thickness+d, r=coinr-3);
}



difference() {
//translate([0,-110,0]) cube([70,110,thickness]);
scale([1,-1,1])
linear_extrude(thickness) {
    offset(r=10) {
    offset(r=-10) {
    polygon([[0,2],[31,8],[52,0],[64,1
    ],[68,14],[49,52],[47,81],[21,112],[7,105],[0,88]]);
    }
}
}

translate([0,-297,-1]) 
linear_extrude(2.5) {
import("gtr-scratchboard-wires-r14.dxf",convexity=200);
}

translate([10,-15,0]) coin();
translate([10,-40,0]) coin();
translate([10,-65,0]) coin();

translate([32,-19,0]) coin();
translate([40,-48,0]) coin();
translate([36,-75,0]) coin();
translate([18,-99,0]) coin();

translate([55,-12,0]) coin();

translate([50,-31,-d/2]) cylinder(h=thickness+d, r=ledr+2*tol);

}