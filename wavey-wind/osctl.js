var oscPort = new osc.WebSocketPort({
    url: location.origin.replace(/https?/, 'ws'),
    metadata: true
});

oscPort.open();

oscPort.on("ready", function () {
  console.log("OSC listening!")
  // Example send
  /*
  oscPort.send({
    address: "/carrier/frequency",
    args: [
      {
        type: "f",
        value: 440
      }
    ]
  });
  */
  oscPort.on("message", function (msg) {
    oscCallbacks[msg.address].forEach(cb => cb(msg.args))
  });
});
