# gtr

augmented guitar with esp32 and mpu6050

## deps

last working version of esp32 arduino with current hardware is :
 - https://github.com/espressif/arduino-esp32/releases/tag/2.0.0

### branch of wavey wind

the nodejs glue
 
 - https://git.kompot.si/g1smo/wavey-wind/src/branch/rob

### the esp32 firmware

 - https://git.kompot.si/g1smo/pifcamp-2021/src/branch/master/osc32final

## GPIO / PINS

LEDS

32 (ADC1_4 / TOUCH9)

GND

// OR 33


### ACCELEROMETER/GYRO

#### MPU6050

GPIO: 

21 - SDA

22 - SCL

GND - GND

VCC - 3.3V

### CAPACATIVE TOUCH

GPIO:

momentary switchs

on diagram (0,1,2,3)

4  TOUCH0 - SHIFT KEY

0  TOUCH1 - record to buffer 0

2  TOUCH2

15 TOUCH3

on diagram (4,5,6,7)

13 TOUCH4 

14 TOUCH5

12 TOUCH6

27 TOUCH7

TOUCH0 + TOUCH3		=	EULER ROTATION RESET


array of 16 values -

8 x data inputs +

8 x 0/1 values - first 4 momentary second 4 toggle

args: [
    35, 56, 53, 47, 99, 106, 99, 48,
    0,  0,  0,   0, 0,  0,  0,  0
  ]



## BUTTON MAPPING


BUTTON 0 ----- SHIFT
BUTTON 1 ----- TRIGGER ONESHOT BUFFER RECORD

BUTTON X ----- GRANULAR POSITION MODE
BUTTON X ----- GRANULAR PITCH MODE
BUTTON X ----- GRANULAR IMPULSE MODE

BUTTON X ----> LOOP RECORDING MODE ON/OFF
BUTTON X ----> OVERDUB RECORDING MODE ON/OFF

BUTTON X ----- LIVE SIGNAL / PROCESSED / MIX

BUTTON 0 && X RESET/ZERO SENSORS [in firmware]



