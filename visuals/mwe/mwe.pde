import oscP5.*;
import netP5.*;

OscP5 oscP5;
NetAddress myRemoteLocation;
int x1,x2,m1, m2;
void setup() {
  fullScreen();
  //size(400,400);
  frameRate(8);
  /* start oscP5, listening for incoming messages at port 12000 */
  oscP5 = new OscP5(this,47120);

  myRemoteLocation = new NetAddress("127.0.0.1",47120);
//sc3p5
  oscP5.plug(this,"sc3p5","/sc3p5");
  oscP5.plug(this,"test","/test");
  oscP5.plug(this,"mother1","/mother1");
  oscP5.plug(this,"mother2","/mother2");

}

void sc3p5(int theA, int theB) {
  println("### plug event method. received a message /test.");
  println(" 2 ints received: "+theA+", "+theB);
  x1= theA;
  x2= theB;
}

void mother1(int theC) {
  m1= theC;
}

void mother2(int theC) {
  m2= theC;
}

void draw() {
  background(0);
  rect(m1,0 ,5,height);
  rect(0,m2,width, 5);

  //rect(random(0,width),random(0,height),50+x1,50+x2);
}
