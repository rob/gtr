
import oscP5.*;
import netP5.*;
import http.requests.*;
import peasy.*;

PeasyCam cam;
OscP5 oscP5;

float x; // global variable
float y;
float z;

float rx;
float ry;
float rz;

int[] r = {3,5,8,13,21};

PShape chickenfoot;
//String urosC02;

JSONObject json; 
JSONObject jsonchannel; 

JSONArray totalData; 
JSONArray nameData; 
JSONArray chnameData; 

JSONObject record;

int id; 
String name; 

String timeStamp; 
int temp; 
int co2; 
float humidity;
int s; // seconds counter

PFont liberation;
PGraphics urosBuffer;
boolean urosDisplayed;    
       
void setup() {
  fullScreen(P3D);
  //cam = new PeasyCam(this, 100);
  //cam.setMinimumDistance(50);
  //cam.setMaximumDistance(500);

  //// CREATE GRAPHICS BUFFER FOR UROS DATA
  urosBuffer = createGraphics(600,300);
  chickenfoot = loadShape("chicken.obj");
  chickenfoot.setFill(color(200, 0, 0));
  liberation = createFont("LiberationMono-Bold.ttf", 30);
 
  frameRate(24);
  smooth();
  
// SET OSC LISTEN FROM SUPERCOLLIDER
  OscProperties properties = new OscProperties();
  properties.setListeningPort(47120); // osc receive port (from sc)
  oscP5 = new OscP5(this, properties);
}

// SET OSC RECEIVE HANDLES (FROM SUPERCOLLIDER PATTERNS)
void oscEvent(OscMessage msg) {
  if (msg.checkAddrPattern("/sc3p5")) {
    x = msg.get(0).floatValue(); // receive floats from sc
  }
  if (msg.checkAddrPattern("/mother2")) {
    y = msg.get(0).floatValue(); // receive floats from sc
  }
    if (msg.checkAddrPattern("/rotation")) {
    z = msg.get(0).floatValue(); // receive floats from sc
  }
}

// UROS DATA ////////////////////////////////////////

void getUROS() {
    // UROS DATA FROM THINGSPEAK
  json = loadJSONObject("https://api.thingspeak.com/channels/1724587/feeds.json");
  jsonchannel = loadJSONObject("https://api.thingspeak.com/channels/1724587channel.json");

  totalData = json.getJSONArray("feeds");        
  for (int i = 0; i < totalData.size(); i++) { 
     record = totalData.getJSONObject(i); 
     id = record.getInt("entry_id"); 
     timeStamp = record.getString("created_at"); 
     temp = int(record.getString("field1")); 
     co2 = int(record.getString("field3")); 
     humidity = int(record.getString("field4"));          }
 }
 
void urosDrawData() {
  //draw data direct to the buffer
    //urosBuffer.scale(2);

  urosBuffer.beginDraw(); 
    urosBuffer.clear();
    //size(222,111);
    urosBuffer.background(0,0,0, 0.5);
    urosBuffer.noFill();
    //urosBuffer.fill(111);
    //urosBuffer.rect(0,0, width/5, height/5);
    urosBuffer.fill(0);
    urosBuffer.textFont(liberation);
    urosBuffer.text("Location: " + name, 30, 50);
    urosBuffer.text("Time: " + timeStamp, 30, 100);
    urosBuffer.text("Temperature (Celsius): " + temp, 30, 150);
    urosBuffer.text("CO2 PPM: " + co2, 30, 200);
    urosBuffer.text("Humidity  (%rel): " + humidity, 30, 250); 
  urosBuffer.endDraw();
}

void drawTarget(float xloc, float yloc, float size, float num) {
  float grayvalues = 255/num;
  float steps = size/num;
  ellipseMode(CENTER);
  for (int i = 0; i < num; i++) {
    fill(i*grayvalues);
    ellipse(xloc, yloc, size - i*steps, size - i*steps);
    }
  }

////////////////////////////////////////////////////////

void draw() {

  if (y == 3){
      background(0); 
    } else { 
    background(255); 
  }
  
  // UROS 
  int s = second(); 
  s = s - 60; // countdown timer
  if (s==-1) { // every minute
    thread("getUROS"); // threaded pull of UROS data from thingspeak
  }
  
 // scale(3);
  urosDrawData(); // draw the data into a imagebuffer
   //if(urosDisplayed) { // use boolean to draw or not
  image(urosBuffer,0,0); // display the imageBuffer
  //    }
  //////////////////////////////////////
    //scale(-3);

  lights();
  
  //println("rotation: ", z);
  //rectMode(CORNERS);
  //drawTarget(width/6 , (height/8) *-y + random(0,10) +height*0.7 , random(60, 1200), (y%8)+2);
  //scale(7.5);
   //rotateX(PI*z/6);  

  //drawTarget(width/12 * x, height/2,  222, x);
  //drawTarget(width*0.75, y, 120, 6);
  //background(x, x, x);
  //println("POST: ", x);
  // draw rect
  //stroke(256-x/2, 256-x*abs(sin(x)), 256-x/4);
  //sdtkeWeight(random(12));
  fill(256-x/2, 256-x, 256-x*abs(sin(x)));
  //translate(width/2, height/2);
  //rotate(-x); //make recieve osc
  //rect(x%64, x%64, x*abs(sin(x))%128, x*abs(sin(x))%128, 6);
  //fill(random(255));
//rect(width/12 * x, 0, r[int(random(2))], height, 6);
  //translate(width/2, 0);

    if (y == 4){
  rotateX(PI*z/2);
    } else { 
        rotateX(0);
  }
    
  //rotateX(0.5);
  fill(255);
  //rect(0, (height/8) *-y  +height*0.7 , width, random(0, 22), 6);
  translate(width/2, height/2 + 100, -200);
  rotateX(radians(rz));
  rotateY(ry);
  //rotateZ(rz);

  scale(8);
  fill(255, 255,0);
  shape(chickenfoot);
  
  ry += 0.02;
  rx += 0.025;
  rz += 0.15;


}
